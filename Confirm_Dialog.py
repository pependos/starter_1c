
from PyQt5 import QtWidgets, Qt, QtGui, QtCore
from PyQt5.QtWidgets import QDialog

import icons_rc

class Confirm_Dialog(QDialog):
    def __init__(self, Linux, title, question, x, y, parent=None):
        super().__init__(parent)

        self.setWindowTitle(title)
        icon = QtGui.QIcon()
        if title == "Создание администратора информационной базы":
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/manager.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        elif title == "Удаление сервера":
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/server-close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        elif title == "Удаление информационной базы":
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.resize(x, y)

        QBtn = Qt.QDialogButtonBox.Ok | Qt.QDialogButtonBox.Cancel

        self.buttonBox = Qt.QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel(question)
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)
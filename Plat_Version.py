import stat

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import Qt

import os


class Plat_Version(QtWidgets.QMainWindow):
    def __init__(self, Linux, lineEdit_Platform_ver, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.setObjectName("MainWindow")
        self.resize(278, 268)
        self.setMinimumSize(QtCore.QSize(278, 268))
        self.setMaximumSize(QtCore.QSize(278, 268))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_defaults = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_defaults.setGeometry(QtCore.QRect(10, 220, 101, 31))
        self.pushButton_defaults.setMinimumSize(QtCore.QSize(101, 31))
        self.pushButton_defaults.setMaximumSize(QtCore.QSize(101, 31))
        self.pushButton_defaults.setObjectName("pushButton_defaults")
        self.pushButton_defaults.clicked.connect(lambda: self.pushButton_defaults_clicked(lineEdit_Platform_ver))
        self.pushButton_OK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_OK.setGeometry(QtCore.QRect(190, 210, 75, 23))
        self.pushButton_OK.setMinimumSize(QtCore.QSize(75, 23))
        self.pushButton_OK.setMaximumSize(QtCore.QSize(75, 23))
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.pushButton_OK.clicked.connect(lambda: self.pushButton_OK_clicked(lineEdit_Platform_ver))
        self.pushButton_Cancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Cancel.setGeometry(QtCore.QRect(190, 240, 75, 23))
        self.pushButton_Cancel.setMinimumSize(QtCore.QSize(75, 23))
        self.pushButton_Cancel.setMaximumSize(QtCore.QSize(75, 23))
        self.pushButton_Cancel.setObjectName("pushButton_Cancel")
        self.pushButton_Cancel.clicked.connect(self.pushButton_Cancel_clicked)
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 10, 256, 192))
        self.listWidget.setMinimumSize(QtCore.QSize(256, 192))
        self.listWidget.setMaximumSize(QtCore.QSize(256, 192))
        self.listWidget.setObjectName("listWidget")
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)
        self.setWindowModality(Qt.ApplicationModal)

        self.onStartup(Linux)

        self.listWidget.doubleClicked.connect(lambda: self.pushButton_OK_clicked(lineEdit_Platform_ver))

    def onStartup(self, Linux):
        if Linux:
            targetPattern = r"/opt/1cv8/x86_64/"
        else:
            targetPattern = r'c:\Program Files\1cv8'
        files = targetPattern
        if os.path.isdir(files) == True:
            files = os.listdir(targetPattern)
            for file in files:
                try:
                    a = int(file[0:1])
                    if file[1:2] == ".":
                            parent = QtWidgets.QListWidgetItem(self.listWidget)
                            parent.setText(file)
                except:
                    continue
        else:
            a = 1
            print(a)

    def pushButton_OK_clicked(self, lineEdit_Platform_ver):
        indexes = self.listWidget.selectedIndexes()
        if len(indexes) > 0:
            lineEdit_Platform_ver.setText(indexes[0].data())
        self.close()

    def pushButton_Cancel_clicked(self):
        self.close()

    def pushButton_defaults_clicked(self, lineEdit_Platform_ver):
        lineEdit_Platform_ver.setText("8.*")
        self.close()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Выберите версию"))
        self.pushButton_defaults.setText(_translate("MainWindow", "По умолчанию"))
        self.pushButton_OK.setText(_translate("MainWindow", "ОК"))
        self.pushButton_Cancel.setText(_translate("MainWindow", "Отмена"))
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        item = self.listWidget.item(0)
        item.setText(_translate("MainWindow", "8.*"))
        item = self.listWidget.item(1)
        item.setText(_translate("MainWindow", "8.3"))
        self.listWidget.setSortingEnabled(__sortingEnabled)


from PyQt5 import QtCore, QtXml
from PyQt5.QtWidgets import QTreeWidgetItem, QTreeWidget, QHeaderView, QApplication

class Parse(QtXml.QXmlDefaultHandler):
    def __init__(self, root):
        QtXml.QXmlDefaultHandler.__init__(self)
        self._root = root
        self._item = None
        self._text = ''
        self._error = ''

    def startElement(self, namespace, name, qname, attributes):
        if qname == 'node':
            if self._item is not None:
                self._item = QTreeWidgetItem(self._item)
            else:
                self._item = QTreeWidgetItem(self._root)
            self._item.setData(0, QtCore.Qt.UserRole, qname)
            self._item.setText(0, attributes.value('nodeName'))
            if qname == 'node':
                self._item.setExpanded(False)
                self._item.setText(1, attributes.value('level'))
                self._item.setText(2, attributes.value('IB_Name'))
                self._item.setText(3, attributes.value('server'))
                self._item.setText(4, attributes.value('db'))
                self._item.setText(5, attributes.value('db_ID'))
                self._item.setText(6, attributes.value('launch_params'))
                self._item.setText(7, attributes.value('IB_User'))
                self._item.setText(8, attributes.value('Pwd'))
                self._item.setText(9, attributes.value('arch'))
                self._item.setText(10, attributes.value('platf'))
                self._item.setText(11, attributes.value('client_type'))
                self._item.setText(12, attributes.value('orderInList'))
                self._item.setText(13, attributes.value('folder'))
                self._item.setText(14, attributes.value('orderInTree'))
                self._item.setText(15, attributes.value('external'))
                self._item.setText(16, attributes.value('clientConnection'))
                self._item.setText(17, attributes.value('_App'))
                self._item.setText(18, attributes.value('WA'))
                self._item.setText(19, attributes.value('version'))
                self._item.setText(20, attributes.value('configuration'))
                self._item.setCheckState(0, QtCore.Qt.Unchecked)
        self._text = ''
        self
        return True

    def endElement(self, namespace, name, qname):
        if qname == 'root' or qname == 'node':
            if hasattr(self._item, 'parent'):
                self._item = self._item.parent()
        return True

    def characters(self, text):
        self._text += text
        return True

    def fatalError(self, exception):
        print('Parse Error: line %d, column %d: %s' % (
              exception.lineNumber(),
              exception.columnNumber(),
              exception.message(),
              ))
        return False

    def errorString(self):
        return self._error

class ImportXML():
    def __init__(self, tree, xml):
        super().__init__()
        #self.header().setResizeMode(QHeaderView.Stretch)
        tree.setHeaderLabels(["Имя ноды", "Уровень", "Наименование базы", "Сервер", "База", "ИД базы",
                              "Параметры запуска", "Пользователь базы", "Пароль", "Разрядность", "Платформа",
                              "Тип клиента", "orderInList", "Folder", "OrderInTree", "External", "ClientConnection",
                              "App", "WA", "Version", "Конфигурация"])
        tree.setHeaderHidden(False)
        tree.setColumnWidth(0, 200)
        tree.setColumnWidth(1, 60)
        tree.setColumnWidth(2, 200)
        tree.setColumnWidth(4, 150)
        tree.setColumnWidth(5, 220)
        tree.setColumnWidth(6, 120)
        tree.setColumnWidth(7, 150)

        source = QtXml.QXmlInputSource()
        source.setData(xml)
        handler = Parse(tree)
        reader = QtXml.QXmlSimpleReader()
        reader.setContentHandler(handler)
        reader.setErrorHandler(handler)
        reader.parse(source)

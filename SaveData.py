
import os

from xml.etree import cElementTree as etree

class SaveData():
    def __init__(self, Linux, parent=None):
        super().__init__()
        def build(item, root):
            for row in range(item.childCount()):
                child = item.child(row)
                element = etree.SubElement(root, 'node', nodeName=child.text(0), level=child.text(1),
                                           IB_Name=child.text(2), server=child.text(3), db=child.text(4),
                                           db_ID=child.text(5), launch_params=child.text(6),
                                           IB_User=child.text(7), Pwd=child.text(8), arch=child.text(9),
                                           platf=child.text(10), client_type=child.text(11), orderInList=child.text(12), folder=child.text(13),
                                           orderInTree=child.text(14), external=child.text(15), clientConnection=child.text(16),
                                           _App=child.text(17), WA=child.text(18), version=child.text(19),
                                           configuration=child.text(20))
                build(child, element)

        root = etree.Element('root')
        build(parent.invisibleRootItem(), root)
        from xml.dom import minidom
        xml_text = minidom.parseString(etree.tostring(root)).toprettyxml()
        if Linux:
            home = os.path.expanduser("~")
            dir_path = home + "/.config/Starter_1C"
            with open(dir_path + "/" + "db_starter_settings.xml", "w", encoding='utf-8') as xml_file:
                xml_file.write(xml_text)
        else:
            file = os.path.expandvars(r"%APPDATA%\Starter_1C\db_starter_settings.xml")
            with open(file, "w", encoding='utf-8') as xml_file:
                xml_file.write(xml_text)
        xml_file.close()
import os
import platform
import glob

from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QDialog, QMenuBar, QStyle, QInputDialog, QFileDialog, QMessageBox, QMenu, QAction, \
    QSystemTrayIcon
from PyQt5.Qt import QStandardItemModel, QStandardItem
from PyQt5.QtGui import QIcon

from xml.etree import cElementTree as db_settings

import os
from os import path
import platform

from xml.etree import cElementTree as etree

from ImportXML import ImportXML
from SaveData import SaveData
from Confirm_Dialog import Confirm_Dialog

import icons_rc

from settings import settings

newIB = True
Linux = False
Windows = False
SysArch = ""

tray_icon = None


class Ui_MainWindow(QtWidgets.QMainWindow):

    def __init__(self, app, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.popMenu = None
        self.settings_window = None
        icon = QtGui.QIcon()

        self.setObjectName("MainWindow")
        self.resize(863, 606)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QtCore.QSize(402, 16))
        self.label_5.setMaximumSize(QtCore.QSize(402, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(81, 16))
        self.label.setMaximumSize(QtCore.QSize(81, 16))
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(10, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lineEdit_User = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_User.sizePolicy().hasHeightForWidth())
        self.lineEdit_User.setSizePolicy(sizePolicy)
        self.lineEdit_User.setMinimumSize(QtCore.QSize(201, 20))
        self.lineEdit_User.setMaximumSize(QtCore.QSize(201, 20))
        self.lineEdit_User.setObjectName("lineEdit_User")
        self.horizontalLayout.addWidget(self.lineEdit_User)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QtCore.QSize(81, 16))
        self.label_2.setMaximumSize(QtCore.QSize(81, 16))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        spacerItem1 = QtWidgets.QSpacerItem(10, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.lineEdit_User_Pwd = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_User_Pwd.sizePolicy().hasHeightForWidth())
        self.lineEdit_User_Pwd.setSizePolicy(sizePolicy)
        self.lineEdit_User_Pwd.setMinimumSize(QtCore.QSize(201, 20))
        self.lineEdit_User_Pwd.setMaximumSize(QtCore.QSize(201, 20))
        self.lineEdit_User_Pwd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_User_Pwd.setObjectName("lineEdit_User_Pwd")
        self.horizontalLayout_2.addWidget(self.lineEdit_User_Pwd)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.pushButton_Props = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_Props.sizePolicy().hasHeightForWidth())
        self.pushButton_Props.setSizePolicy(sizePolicy)
        self.pushButton_Props.setMinimumSize(QtCore.QSize(100, 49))
        self.pushButton_Props.setMaximumSize(QtCore.QSize(100, 49))
        self.pushButton_Props.setObjectName("pushButton_Props")
        self.pushButton_Props.setDisabled(False)
        self.horizontalLayout_3.addWidget(self.pushButton_Props)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(81, 16))
        self.label_3.setMaximumSize(QtCore.QSize(81, 16))
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_4.addWidget(self.label_3)
        spacerItem2 = QtWidgets.QSpacerItem(10, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.label_Path_To_Base = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_Path_To_Base.sizePolicy().hasHeightForWidth())
        self.label_Path_To_Base.setSizePolicy(sizePolicy)
        self.label_Path_To_Base.setMinimumSize(QtCore.QSize(311, 16))
        self.label_Path_To_Base.setMaximumSize(QtCore.QSize(311, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_Path_To_Base.setFont(font)
        self.label_Path_To_Base.setObjectName("label_Path_To_Base")
        self.horizontalLayout_4.addWidget(self.label_Path_To_Base)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setMinimumSize(QtCore.QSize(81, 16))
        self.label_4.setMaximumSize(QtCore.QSize(81, 16))
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_5.addWidget(self.label_4)
        spacerItem3 = QtWidgets.QSpacerItem(10, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.label_Configuration = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_Configuration.sizePolicy().hasHeightForWidth())
        self.label_Configuration.setSizePolicy(sizePolicy)
        self.label_Configuration.setMinimumSize(QtCore.QSize(311, 16))
        self.label_Configuration.setMaximumSize(QtCore.QSize(311, 16))
        self.label_Configuration.setObjectName("label_Configuration")
        self.horizontalLayout_5.addWidget(self.label_Configuration)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6.addLayout(self.verticalLayout_2)
        spacerItem4 = QtWidgets.QSpacerItem(348, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem4)
        self.gridLayout.addLayout(self.horizontalLayout_6, 0, 0, 1, 2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.lBut_Check_All = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Check_All.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Check_All.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Check_All.setObjectName("lBut_Check_All")
        self.verticalLayout_3.addWidget(self.lBut_Check_All)
        self.lBut_Unchek_All = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Unchek_All.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Unchek_All.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Unchek_All.setObjectName("lBut_Unchek_All")
        self.verticalLayout_3.addWidget(self.lBut_Unchek_All)
        self.lBut_List_Mgt = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_List_Mgt.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_List_Mgt.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_List_Mgt.setObjectName("lBut_List_Mgt")
        self.verticalLayout_3.addWidget(self.lBut_List_Mgt)
        self.lBut_Set_to_All = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Set_to_All.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Set_to_All.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Set_to_All.setObjectName("lBut_Set_to_All")
        self.verticalLayout_3.addWidget(self.lBut_Set_to_All)
        self.lBut_Add_IB = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Add_IB.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Add_IB.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Add_IB.setObjectName("lBut_Add_IB")
        self.verticalLayout_3.addWidget(self.lBut_Add_IB)
        self.lBut_Del_IB = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Del_IB.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Del_IB.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Del_IB.setObjectName("lBut_Del_IB")
        self.verticalLayout_3.addWidget(self.lBut_Del_IB)
        self.lBut_Clear_Cache = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Clear_Cache.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Clear_Cache.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Clear_Cache.setObjectName("lBut_Clear_Cache")
        self.verticalLayout_3.addWidget(self.lBut_Clear_Cache)
        self.lBut_Up = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Up.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Up.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Up.setObjectName("lBut_Up")
        self.verticalLayout_3.addWidget(self.lBut_Up)
        self.lBut_Down = QtWidgets.QPushButton(self.centralwidget)
        self.lBut_Down.setMinimumSize(QtCore.QSize(31, 31))
        self.lBut_Down.setMaximumSize(QtCore.QSize(31, 31))
        self.lBut_Down.setObjectName("lBut_Down")
        self.verticalLayout_3.addWidget(self.lBut_Down)
        spacerItem5 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem5)
        self.gridLayout.addLayout(self.verticalLayout_3, 1, 0, 1, 1)
        self.treeWidget = QtWidgets.QTreeWidget(self.centralwidget)
        self.treeWidget.setObjectName("treeWidget")
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.treeWidget.headerItem().setFont(2, font)
        self.gridLayout.addWidget(self.treeWidget, 1, 1, 1, 1)
        self.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.treeWidget.doubleClicked.connect(self.doubleClick)
        self.treeWidget.clicked.connect(self.lineClick)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

        self.onStartup()
        self.treeWidget.setCurrentIndex(self.treeWidget.model().index(0, 0))

        self.treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.treeWidget.customContextMenuRequested.connect(self.create_context_menu_treeview)

        self.setIcons()

        self.already_shown = False

        icon = QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/server-setting.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)

        self.trayIcon = QSystemTrayIcon(QIcon(r":/AdminConsole/Icons/server-setting.png"), self)

        self.trayIcon.activated.connect(self.onTrayIconActivated)
        self.trayIcon.setToolTip("Стартер 1С")
        self.trayIcon.show()
        self.disambiguateTimer = QtCore.QTimer(self)
        self.disambiguateTimer.setSingleShot(True)
        self.disambiguateTimer.timeout.connect(self.disambiguateTimerTimeout)
        menu = QMenu()
        show_main_window = QAction(QIcon(''), "Показать основное окно", menu)
        show_main_window.triggered.connect(self.show_main_window)
        menu.addAction(show_main_window)
        eexit = QAction(QIcon(''), "Выход", menu)
        eexit.triggered.connect(self.exit_question)
        menu.addAction(eexit)
        self.trayIcon.setContextMenu(menu)

        self.pushButton_connectors()

    def pushButton_connectors(self):
        self.lBut_Down.clicked.connect(lambda: self.moveUpDown(+1))
        self.lBut_Up.clicked.connect(lambda: self.moveUpDown(-1))
        self.lBut_Unchek_All.clicked.connect(lambda: self.check_uncheck_all(False))
        self.lBut_Check_All.clicked.connect(lambda: self.check_uncheck_all(True))
        self.pushButton_Props.clicked.connect(self.open_settings_window)
        self.lineEdit_User_Pwd.editingFinished.connect(self.editUserPwdFinished)
        self.lineEdit_User.editingFinished.connect(self.editUserFinished)
        self.lBut_List_Mgt.clicked.connect(self.show_list_mgt_menu)

    def show_list_mgt_menu(self):
        point = QtCore.QPoint(self.lBut_List_Mgt.pos().x() + 30, self.lBut_List_Mgt.pos().y() - 190)
        self.popMenu = QMenu()
        self.popMenu.addAction(QAction("Открыть папку локального кэша", self.lBut_List_Mgt))
        self.popMenu.addAction(QAction("Открыть папку пользовательского кэша", self.lBut_List_Mgt))
        self.popMenu.addSeparator()
        self.popMenu.addAction(QAction('test2', self.lBut_List_Mgt))
        rez = self.popMenu.exec_(self.lBut_List_Mgt.mapToGlobal(point))
        if rez.text() == "Открыть папку локального кэша":
            pass
        elif rez.text() == "Открыть папку пользовательского кэша":
            pass

    def closeEvent(self, event):
        event.ignore()
        if self.already_shown == False:
            self.trayIcon.showMessage(r"Стартер 1С",
                                      r"Программа свёрнута в трей и продолжает работать. Щелкните по значку приложения в трее, чтобы открыть главное окно", )
            self.already_shown = True
        self.hide()

    def onTrayIconActivated(self, reason):
        # print("onTrayIconActivated:", reason)
        if reason == QSystemTrayIcon.Trigger:
            # self.disambiguateTimer.start(QtWidgets.QApplication.doubleClickInterval())
            self.show()
        elif reason == QSystemTrayIcon.DoubleClick:
            self.disambiguateTimer.stop()
            # print("Tray icon double clicked")

    def disambiguateTimerTimeout(self):
        # print("Tray icon single clicked")
        a = 1

    def show_main_window(self):
        self.show()

    def exit_question(self):
        reply = QMessageBox.question(self, 'Информация', "Вы уверены, что хотите уйти?", QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.trayIcon.hide()
            SaveData(Linux, self.treeWidget)
            self.close()
        else:
            a = 1

    def onStartup(self):
        global newIB
        global Linux
        Linux = False
        global Windows
        Windows = False
        global SysArch
        from os.path import expanduser
        # home = expanduser("~")
        # print(home)
        # print(os.name)
        SysType = platform.system()
        SysArch = platform.architecture()
        # print(SysType)
        # print(SysArch[0])

        if SysType == "Windows" and SysArch[0] == "32bit":
            Windows = True
        elif SysType == "Windows" and SysArch[0] == "64bit":
            Windows = True
        elif SysType == "Linux":
            Linux = True

        if Linux:
            home = os.path.expanduser("~")
            dir_path = home + "/.config/Starter_1C"
            file = "db_starter_settings.xml"
            if os.path.isfile(os.path.expandvars(dir_path + r"/" + file)):
                self.do_import_xml(dir_path + r"/" + file)
            else:
                ok = False
                if not os.path.isdir(dir_path):
                    try:
                        access_rights = 0o755
                        os.mkdir(dir_path, access_rights)
                        ok = True
                    except OSError:
                        self.create_dir_error(dir_path)
                    if ok:
                        db_starter_settings = open(os.path.expandvars(dir_path + r"/" + file), "w")
                        db_starter_settings.write("")
                        db_starter_settings.close()
                        self.read_v8i()
                else:
                    db_starter_settings = open(os.path.expandvars(dir_path + r"/" + file), "w")
                    db_starter_settings.write("")
                    db_starter_settings.close()
                    self.read_v8i()
        else:
            dir_path = os.path.expandvars(r"%APPDATA%\Starter_1C\\")
            file = "db_starter_settings.xml"
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            if os.path.isfile(os.path.expandvars(r"%APPDATA%\Starter_1C\db_starter_settings.xml")):
                self.do_import_xml(dir_path + file)
            else:
                win_path = os.path.expandvars("%APPDATA%")
                if os.path.isdir(win_path + "\Starter_1C"):
                    db_starter_settings = open(win_path + "\Starter_1C\db_starter_settings.xml", "w")
                    db_starter_settings.write("")
                    db_starter_settings.close()
                    self.read_v8i()
                else:
                    try:
                        access_rights = 0o755
                        win_path = os.path.expandvars("%APPDATA%")
                        os.mkdir(win_path + r"\Starter_1C")
                        ok = True
                    except OSError:
                        self.create_dir_error(win_path + r"\Starter_1C")
                    if ok:
                        db_starter_settings = open(win_path + "\Starter_1C\db_starter_settings.xml", "w")
                        db_starter_settings.write("")
                        db_starter_settings.close()
                        self.read_v8i()
        for a in range(self.treeWidget.columnCount()):
            self.treeWidget.resizeColumnToContents(a)

    def setIcons(self):
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/check-mark-box-line.png"), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
        self.lBut_Check_All.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/check-unmark-box-line.png"), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
        self.lBut_Unchek_All.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/hamburger-menu.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_List_Mgt.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/magic.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Set_to_All.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/plus-round-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Add_IB.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/trash-bin.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Del_IB.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/sweeper-cleaning.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Clear_Cache.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/line-angle-up.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Up.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/line-angle-down.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.lBut_Down.setIcon(icon)

    def create_dir_error(self, dir):
        warning_text = "Не удалось создать директорию %s" % os.path.expandvars(dir)
        QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)

    def read_v8i(self):
        # %APPDATA%          возвращает "AppData\Roaming"
        # %LOCALAPPDATA%     возвращает "AppData\Local"
        # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
        if Windows:
            ibases_v8i = path.expandvars(r"%APPDATA%\1C\1CEStart\ibases.v8i")
        else:
            ibases_v8i = path.expandvars(r"/home/${USER}/.1C/1cestart/ibases.v8i")
        with open(ibases_v8i, encoding='utf-8') as read_file:
            a = 1
            row = 0
            while True:
                line = read_file.readline().strip()
                if len(line) > 0:
                    if a == 1 and line[1] == "[":
                        row = row + 1
                        newIB = True
                        self.stringCheck(line, a, row)
                        a = 0
                    elif a == 0 and line[0] == "[":
                        row = row + 1
                        newIB = True
                        self.stringCheck(line, a, row)
                        a = 0
                    else:
                        newIB = False
                        self.stringCheck(line, a, row)
                if not line:
                    break
        read_file.close()
        self.treeWidget.sortItems(12, Qt.Qt.AscendingOrder)

    def stringCheck(self, sstring, a, row):
        global newIB
        # print(sstring)
        if a == 1:
            if sstring[1] == "[" and newIB == True:
                self.treeWidget.model().rowCount()
                self.add_Line(sstring, a, row)
                newIB = False
        else:
            if sstring[0] == "[":
                row = self.treeWidget.model().rowCount()
                self.add_Line(sstring, a, row)
            else:
                self.add_To_Line(sstring, a, row)

    def add_Line(self, sstring, a, row):
        nameIB = sstring[a + 1:len(sstring) - 1]
        parent = QtWidgets.QTreeWidgetItem(self.treeWidget)
        parent.setText(0, nameIB)
        parent.setText(2, nameIB)
        parent.setCheckState(0, QtCore.Qt.Unchecked)

    def add_To_Line(self, sstring, a, row):
        if sstring[0:7] == "Connect":
            dot_quote = sstring.find(";", 0)
            srvr = sstring[14:dot_quote - 1]
            ref = sstring[dot_quote + 6:len(sstring) - 2]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(3, srvr)
            parent.setText(4, ref)
        elif sstring[0:2] == "ID":
            ib_id = sstring[3:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(5, ib_id)
        elif sstring[0:11] == "OrderInList":
            id = sstring[12:len(sstring)]
            id = float(id)
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setData(12, 0, id)  # setText(12, id)
        elif sstring[0:6] == "Folder":
            id = sstring[7:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(13, id)
        elif sstring[0:11] == "OrderInTree":
            id = sstring[12:len(sstring)]
            id = float(id)
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setData(14, 0, id)
        elif sstring[0:8] == "External":
            id = sstring[9:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(15, id)
        elif sstring[0:21] == "ClientConnectionSpeed":
            id = sstring[22:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(16, id)
        elif sstring[0:4] == "App=":
            id = sstring[4:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(17, id)
        elif sstring[0:2] == "WA":
            id = sstring[3:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(18, id)
        elif sstring[0:7] == "Version":
            id = sstring[8:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(19, id)
        elif sstring[0:7] == "AppArch":
            id = sstring[8:len(sstring)]
            row = self.treeWidget.model().rowCount()
            parent = self.treeWidget.topLevelItem(row - 1)
            parent.setText(20, id)

    def open_settings_window(self):
        currNode = self.treeWidget.currentItem()
        self.settings_window = settings(Linux, currNode, self.treeWidget)
        self.settings_window.show()

    def lineClick(self):
        indexes = self.treeWidget.selectedIndexes()
        self.lineEdit_User.setText(self.treeWidget.itemFromIndex(indexes[0]).data(7, 0))
        self.lineEdit_User_Pwd.setText(self.treeWidget.itemFromIndex(indexes[0]).data(8, 0))
        self.pushButton_Props.setDisabled(False)

    def doubleClick(self):
        currNode = self.treeWidget.currentItem()
        _enterprise = True
        _designer = False
        self.launch1C(currNode, _enterprise, _designer)
        if not self.already_shown:
            self.trayIcon.showMessage(r"Стартер 1С", r"Программа свёрнута в трей и продолжает работать. Щелкните по "
                                                     r"значку приложения в трее, чтобы открыть главное окно", )
            self.already_shown = True
        self.hide()

    def launch1C(self, currNode, _enterprise=False, _designer=False):
        version = currNode.data(19, 0)
        launch_params = currNode.data(6, 0)
        if Linux:
            targetPattern = r"/opt/1cv8/x86_64/" + version
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                LastPath = Files[0] + "/1cestart"
            error_file = "/tmp/launch_ib_errors.txt"
            error_file_string = " 2>\"" + error_file + "\""
        else:
            targetPattern = r"C:\Program Files\1cv8\common\1cestart.exe"
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                LastPath = Files[0]
            win_path = os.path.expandvars("%LOCALAPPDATA%")
            error_file = win_path + r"\Temp\launch_ib_errors.txt"
            error_file_string = " 2>" + error_file
        # /opt/1cv8/x86_64/8.3.20.1613/1cestart

        if _enterprise:
            ent_des_text = "ENTERPRISE"
        elif _designer:
            ent_des_text = "DESIGNER"
        if currNode.data(4, 0) != "":  # Серверная база
            srvr = currNode.data(3, 0)
            ref = currNode.data(4, 0)
            if currNode.data(7, 0) is None:
                command = "\"" + "\"" + LastPath + "\"" + "\"" + " " + ent_des_text + launch_params + " " + r"/S" +\
                          srvr + "\\" + ref + error_file_string
            else:
                user = currNode.data(7, 0)
                if currNode.data(8, 0) is None:
                    command = "\"" + "\"" + LastPath + "\"" + " " + ent_des_text + launch_params + " " + r"/S" + \
                              srvr + "\\" + ref + " " + r"/N" + "\"" + user + "\"" + "\"" + error_file_string
                else:
                    pwd = currNode.data(8, 0)
                    command = "\"" + "\"" + LastPath + "\"" + " " + ent_des_text + launch_params + " " + r"/S" + srvr +\
                              "\\" + ref + " " + r"/N" + "\"" + user + "\"" + " " + r"/P" + "\"" + pwd + "\"" + "\"" +\
                              error_file_string
        else:
            dir_path = currNode.data(3, 0)
            if currNode.data(7, 0) is None:
                command = "\"" + "\"" + LastPath + "\"" + "\"" + " " + ent_des_text + launch_params + " " + r"/F" +\
                          dir_path + error_file_string
            else:
                user = currNode.data(7, 0)
                if currNode.data(8, 0) is None:
                    command = "\"" + "\"" + LastPath + "\"" + " " + ent_des_text + launch_params + " " + r"/F" + \
                              dir_path + " " + r"/N" + "\"" + user + "\"" + "\"" + error_file_string
                else:
                    pwd = currNode.data(8, 0)
                    command = "\"" + "\"" + LastPath + "\"" + " " + ent_des_text + launch_params + " " + r"/F" + \
                              dir_path + " " + r"/N" + "\"" + user + "\"" + " " + r"/P" + "\"" + pwd + "\"" + "\"" +\
                              error_file_string
        os.system(command)
        f = open(error_file, 'r')
        ErrorsData = f.read()
        if Linux:
            pass
        else:
            ErrorsData = ErrorsData.encode("cp1251")
            ErrorsData = ErrorsData.decode("cp866")

        if not ErrorsData == "":
            QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
            f.close()
            os.remove(error_file)
            return
        else:
            f.close()
            os.remove(error_file)
        self.hide()

    def editUserFinished(self):
        currNode = self.treeWidget.currentItem()
        currNode.setData(7, 0, self.lineEdit_User.text())
        SaveData(Linux, self.treeWidget)

    def editUserPwdFinished(self):
        currNode = self.treeWidget.currentItem()
        currNode.setData(8, 0, self.lineEdit_User_Pwd.text())
        SaveData(Linux, self.treeWidget)

    def do_import_xml(self, file):
        with open(file, encoding='utf-8') as xml_file:
            xml_text = xml_file.read()
            if len(xml_text) > 0:
                ImportXML(self.treeWidget, xml_text)
            else:
                self.read_v8i()
        xml_file.close()

    def create_context_menu_treeview(self, event):
        self.lineClick()

        ix = self.treeWidget.indexAt(event)
        if ix.column() == -1:
            self.add_new_ib_context_menu = QMenu(self.treeWidget)
            firstMenu = self.add_new_ib_context_menu.addAction("Добавить базу...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            firstMenu.setIcon(icon)
            secondMenu = self.add_new_ib_context_menu.addAction("Добавить группу...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/add_folder.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            secondMenu.setIcon(icon)
            action = self.add_new_ib_context_menu.exec_(self.treeWidget.mapToGlobal(event))
            if action is not None:
                if action == firstMenu:
                    pass
                    # self.create_server_window()
        else:
            user_cash_menu = ""
            clear_user_cash_menu = ""
            treeModel = self.treeWidget.model()
            index = treeModel.index(0, 0)
            self.context_menu = QMenu(self.treeWidget)
            firstMenu = self.context_menu.addAction("1С: Предприятие")
            secondMenu = self.context_menu.addAction("Конфигуратор")
            first_separator = self.context_menu.addSeparator()
            if Linux:
                local_cash_menu = self.context_menu.addAction("Открыть папку кэша")
            else:
                local_cash_menu = self.context_menu.addAction("Открыть папку локального кэша")
                user_cash_menu = self.context_menu.addAction("Открыть папку пользовательского кэша")
            second_separator = self.context_menu.addSeparator()
            if Linux:
                clear_local_cash_menu = self.context_menu.addAction("Удалить кэш")
            else:
                clear_local_cash_menu = self.context_menu.addAction("Удалить локальный кэш")
                clear_user_cash_menu = self.context_menu.addAction("Удалить пользовательский кэш")

            action = self.context_menu.exec_(self.treeWidget.mapToGlobal(event))
            if action is not None:
                currNode = self.treeWidget.currentItem()
                if action == firstMenu:
                    enterprise = True
                    designer = False
                    self.launch1C(currNode, enterprise, designer)
                elif action == secondMenu:
                    enterprise = False
                    designer = True
                    self.launch1C(currNode, enterprise, designer)
                elif action == local_cash_menu:
                    import webbrowser
                    ib_id = currNode.data(5, 0)
                    if Linux:
                        home = os.path.expanduser("~")
                        dir_path = home + "/.1cv8/1C/1cv8/" + ib_id
                    else:
                        dir_path = os.path.expandvars("%LOCALAPPDATA%") + r"\1C\1Cv8\\" + ib_id
                    webbrowser.open(dir_path)
                elif action == user_cash_menu:
                    import webbrowser
                    ib_id = currNode.data(5, 0)
                    dir_path = os.path.expandvars("%APPDATA%") + r"\1C\1Cv8\\" + ib_id
                    webbrowser.open(dir_path)
                elif action == clear_local_cash_menu:
                    title = "Очистка кэша"
                    if Linux:
                        question = "Действительно удалить кэш?"
                    else:
                        question = "Действительно удалить локальный кэш?"
                    x = 320
                    y = 100
                    ex = Confirm_Dialog(Linux, title, question, x, y, self.treeWidget)
                    if ex.exec():
                        ib_id = currNode.data(5, 0)
                        if Linux:
                            home = os.path.expanduser("~")
                            dir_path = home + "/.1cv8/1C/1cv8/" + ib_id
                        else:
                            dir_path = os.path.expandvars("%LOCALAPPDATA%") + r"\1C\1Cv8" + "\\" + ib_id
                        import shutil
                        shutil.rmtree(dir_path)
                    else:
                        return
                elif action == clear_user_cash_menu:
                    title = "Очистка кэша"
                    question = "Действительно удалить пользовательский кэш?"
                    x = 320
                    y = 100
                    ex = Confirm_Dialog(Linux, title, question, x, y, self.treeWidget)
                    if ex.exec():
                        ib_id = currNode.data(5, 0)
                        if Linux:
                            home = os.path.expanduser("~")
                            dir_path = home + "/.1cv8/1C/1cv8/" + ib_id
                        else:
                            dir_path = os.path.expandvars("%APPDATA%") + r"\1C\1Cv8" + "\\" + ib_id
                        import shutil
                        shutil.rmtree(dir_path)
                    else:
                        return

    def check_uncheck_all(self, check):
        root = self.treeWidget.invisibleRootItem()
        child_count = root.childCount()
        for i in range(child_count):
            item = root.child(i)
            if check:
                item.setCheckState(0, QtCore.Qt.Checked)
            else:
                item.setCheckState(0, QtCore.Qt.Unchecked)

    def moveUpDown(self, upDown):
        item = self.treeWidget.currentItem()
        row = self.treeWidget.currentIndex().row()
        if upDown == 1:
            if item is not None and row < (self.treeWidget.model().rowCount() - 1):
                self.treeWidget.takeTopLevelItem(row)
                self.treeWidget.insertTopLevelItem(row + upDown, item)
                self.treeWidget.setCurrentItem(item)
        else:
            if item is not None and row > 0:
                self.treeWidget.takeTopLevelItem(row)
                self.treeWidget.insertTopLevelItem(row + upDown, item)
                self.treeWidget.setCurrentItem(item)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Стартер 1С"))
        self.label_5.setText(_translate("MainWindow", "Данные для входа в базу"))
        self.label.setText(_translate("MainWindow", "Пользователь:"))
        self.label_2.setText(_translate("MainWindow", "Пароль:"))
        self.pushButton_Props.setText(_translate("MainWindow", "Свойства базы"))
        self.label_3.setText(_translate("MainWindow", "Путь к базе:"))
        self.label_Path_To_Base.setText(_translate("MainWindow", "Какой-то текст"))
        self.label_4.setText(_translate("MainWindow", "Конфигурация:"))
        self.label_Configuration.setText(_translate("MainWindow", "Какой-то текст"))
        self.lBut_Check_All.setToolTip(_translate("MainWindow", "Отметить все базы"))
        # self.lBut_Check_All.setText(_translate("MainWindow", "1"))
        self.lBut_Unchek_All.setToolTip(_translate("MainWindow", "Снять отметки со всех баз"))
        # self.lBut_Unchek_All.setText(_translate("MainWindow", "2"))
        self.lBut_List_Mgt.setToolTip(_translate("MainWindow", "Показать меню управления списком"))
        # self.lBut_List_Mgt.setText(_translate("MainWindow", "3"))
        self.lBut_Set_to_All.setToolTip(_translate("MainWindow", "Установить свойства для всех отмеченных баз"))
        # self.lBut_Set_to_All.setText(_translate("MainWindow", "4"))
        self.lBut_Add_IB.setToolTip(_translate("MainWindow", "Добавить [Ins]"))
        # self.lBut_Add_IB.setText(_translate("MainWindow", "5"))
        self.lBut_Del_IB.setToolTip(_translate("MainWindow", "Удалить [Del]"))
        # self.lBut_Del_IB.setText(_translate("MainWindow", "6"))
        self.lBut_Clear_Cache.setToolTip(_translate("MainWindow", "Открыть диалог очистки временных файлов"))
        # self.lBut_Clear_Cache.setText(_translate("MainWindow", "7"))
        self.lBut_Up.setToolTip(_translate("MainWindow", "Переместить вверх"))
        # self.lBut_Up.setText(_translate("MainWindow", "8"))
        self.lBut_Down.setToolTip(_translate("MainWindow", "Переместить вниз"))
        # self.lBut_Down.setText(_translate("MainWindow", "9"))
        self.treeWidget.headerItem().setText(0, _translate("MainWindow", "Имя ноды"))
        self.treeWidget.headerItem().setText(1, _translate("MainWindow", "Уровень"))
        self.treeWidget.headerItem().setText(2, _translate("MainWindow", "Наименование базы"))
        self.treeWidget.headerItem().setText(3, _translate("MainWindow", "Сервер"))
        self.treeWidget.headerItem().setText(4, _translate("MainWindow", "База"))
        self.treeWidget.headerItem().setText(5, _translate("MainWindow", "ИД Базы"))
        self.treeWidget.headerItem().setText(6, _translate("MainWindow", "Параметры запуска"))
        self.treeWidget.headerItem().setText(7, _translate("MainWindow", "Пользователь базы"))
        self.treeWidget.headerItem().setText(8, _translate("MainWindow", "Пароль"))
        self.treeWidget.headerItem().setText(9, _translate("MainWindow", "Разрядность"))
        self.treeWidget.headerItem().setText(10, _translate("MainWindow", "Платформа"))
        self.treeWidget.headerItem().setText(11, _translate("MainWindow", "Режим работы"))
        self.treeWidget.headerItem().setText(12, _translate("MainWindow", "OrderInList"))
        self.treeWidget.headerItem().setText(13, _translate("MainWindow", "Folder"))
        self.treeWidget.headerItem().setText(14, _translate("MainWindow", "OrderInTree"))
        self.treeWidget.headerItem().setText(15, _translate("MainWindow", "External"))
        self.treeWidget.headerItem().setText(16, _translate("MainWindow", "ClientConnectionSpeed"))
        self.treeWidget.headerItem().setText(17, _translate("MainWindow", "App"))
        self.treeWidget.headerItem().setText(18, _translate("MainWindow", "WA"))
        self.treeWidget.headerItem().setText(19, _translate("MainWindow", "Version"))
        self.treeWidget.headerItem().setText(20, _translate("MainWindow", "AppArch"))
        self.treeWidget.headerItem().setText(21, _translate("MainWindow", "Конфигурация"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_MainWindow(app)
    ui.show()
    sys.exit(app.exec_())
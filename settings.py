from PyQt5 import Qt, QtCore, QtGui, QtWidgets
import os
from PyQt5.QtWidgets import QFileDialog
from ParametriZapuska import ParametriZapuska
from Plat_Version import Plat_Version
from SaveData import SaveData


class settings(QtWidgets.QMainWindow):
    def __init__(self, Linux, currNode, parent=None):
        super().__init__(parent, QtCore.Qt.Window)

        self.prop_window = None
        self.setObjectName("MainWindow")
        self.resize(700, 540)
        self.setMinimumSize(QtCore.QSize(700, 540))
        self.setMaximumSize(QtCore.QSize(700, 540))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/newPrefix/Icons/check-mark-box-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(False)
        font.setWeight(50)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 70, 150, 16))
        self.label_2.setMinimumSize(QtCore.QSize(150, 16))
        self.label_2.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.lineEdit_Configuration = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Configuration.setGeometry(QtCore.QRect(180, 70, 455, 20))
        self.lineEdit_Configuration.setMinimumSize(QtCore.QSize(455, 20))
        self.lineEdit_Configuration.setMaximumSize(QtCore.QSize(455, 20))
        self.lineEdit_Configuration.setObjectName("lineEdit_Configuration")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 120, 150, 16))
        self.label_3.setMinimumSize(QtCore.QSize(150, 16))
        self.label_3.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.lineEdit_IB_Name = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_IB_Name.setGeometry(QtCore.QRect(180, 120, 410, 20))
        self.lineEdit_IB_Name.setMinimumSize(QtCore.QSize(410, 20))
        self.lineEdit_IB_Name.setMaximumSize(QtCore.QSize(410, 20))
        self.lineEdit_IB_Name.setObjectName("lineEdit_IB_Name")
        self.lineEdit_IB_Name.setReadOnly(True)

        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(310, 330, 110, 20))
        self.comboBox.setMinimumSize(QtCore.QSize(130, 20))
        self.comboBox.setMaximumSize(QtCore.QSize(130, 20))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        self.pushButtonOK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonOK.setGeometry(QtCore.QRect(520, 480, 75, 30))
        self.pushButtonOK.setMinimumSize(QtCore.QSize(75, 30))
        self.pushButtonOK.setMaximumSize(QtCore.QSize(75, 30))
        self.pushButtonOK.setObjectName("pushButtonOK")

        self.pushButtonOK.clicked.connect(lambda: self.button_ok_clicked(currNode, Linux, parent))

        self.pushButton_Cancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Cancel.setGeometry(QtCore.QRect(610, 480, 75, 30))
        self.pushButton_Cancel.setMinimumSize(QtCore.QSize(75, 30))
        self.pushButton_Cancel.setMaximumSize(QtCore.QSize(75, 30))
        self.pushButton_Cancel.setObjectName("pushButton_Cancel")

        self.pushButton_Cancel.clicked.connect(self.button_cancel_clicked)

        self.pushButton_Plat_ver = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Plat_ver.setGeometry(QtCore.QRect(450, 330, 50, 20))
        self.pushButton_Plat_ver.setMinimumSize(QtCore.QSize(50, 20))
        self.pushButton_Plat_ver.setMaximumSize(QtCore.QSize(50, 20))
        self.pushButton_Plat_ver.setObjectName("pushButton_Plat_ver")
        self.pushButton_Plat_ver.clicked.connect(lambda: self.plat_ver_clicked(Linux))
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 200, 150, 16))
        self.label_4.setMinimumSize(QtCore.QSize(150, 16))
        self.label_4.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(20, 380, 150, 16))
        self.label_5.setMinimumSize(QtCore.QSize(150, 16))
        self.label_5.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.lineEdit_DB_Path_File = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_DB_Path_File.setGeometry(QtCore.QRect(180, 200, 410, 20))
        self.lineEdit_DB_Path_File.setMinimumSize(QtCore.QSize(410, 20))
        self.lineEdit_DB_Path_File.setMaximumSize(QtCore.QSize(410, 20))
        self.lineEdit_DB_Path_File.setObjectName("lineEdit_DB_Path_File")
        self.lineEdit_DB_Path_File.setReadOnly(True)

        self.pushButton_DB_Path_File = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_DB_Path_File.setGeometry(QtCore.QRect(605, 190, 32, 32))
        self.pushButton_DB_Path_File.setMinimumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_Path_File.setMaximumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_Path_File.setText("...")
        self.pushButton_DB_Path_File.setObjectName("pushButton_DB_Path_File")
        self.pushButton_DB_Path_File.setEnabled(False)
        self.pushButton_DB_Path_File.clicked.connect(self.pushButton_DB_Path_File_clicked)


        self.pushButton_IB_Name_Lock = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_IB_Name_Lock.setGeometry(QtCore.QRect(650, 110, 32, 32))
        self.pushButton_IB_Name_Lock.setMinimumSize(QtCore.QSize(32, 32))
        self.pushButton_IB_Name_Lock.setMaximumSize(QtCore.QSize(32, 32))
        self.pushButton_IB_Name_Lock.setText("")
        self.pushButton_IB_Name_Lock.setObjectName("pushButton_IB_Name_Lock")
        self.pushButton_IB_Name_Lock.clicked.connect(lambda: self.lock_unlock(self.pushButton_IB_Name_Lock,
                                                                              self.lineEdit_IB_Name, Linux))

        self.pushButton_IB_Name_Lock.locked = True
        self.pushButton_DB_PATH_Lock = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_DB_PATH_Lock.setGeometry(QtCore.QRect(650, 190, 32, 32))
        self.pushButton_DB_PATH_Lock.setMinimumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_PATH_Lock.setMaximumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_PATH_Lock.setText("")
        self.pushButton_DB_PATH_Lock.setObjectName("pushButton_DB_PATH_Lock")
        self.pushButton_DB_PATH_Lock.clicked.connect(lambda: self.lock_unlock(self.pushButton_DB_PATH_Lock,
                                                                              self.lineEdit_DB_Path_File, Linux))
        self.pushButton_DB_PATH_Lock.locked = True
        self.comboBox_2 = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_2.setGeometry(QtCore.QRect(180, 380, 450, 20))
        self.comboBox_2.setMinimumSize(QtCore.QSize(450, 20))
        self.comboBox_2.setMaximumSize(QtCore.QSize(450, 20))
        self.comboBox_2.setObjectName("comboBox_2")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(20, 430, 150, 16))
        self.label_6.setMinimumSize(QtCore.QSize(150, 16))
        self.label_6.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.lineEdit_Start_Params = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Start_Params.setGeometry(QtCore.QRect(180, 430, 455, 20))
        self.lineEdit_Start_Params.setMinimumSize(QtCore.QSize(455, 20))
        self.lineEdit_Start_Params.setMaximumSize(QtCore.QSize(455, 20))
        self.lineEdit_Start_Params.setObjectName("lineEdit_Start_Params")

        # self.lineEdit_Configuration.setText(currNode.data(3, 0))

        self.pushButton_Start_params = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Start_params.setGeometry(QtCore.QRect(650, 420, 32, 32))
        self.pushButton_Start_params.setMinimumSize(QtCore.QSize(32, 32))
        self.pushButton_Start_params.setMaximumSize(QtCore.QSize(32, 32))
        self.pushButton_Start_params.setObjectName("pushButton_Start_params")
        self.pushButton_Start_params.clicked.connect(lambda: self.open_prop_window(currNode))
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(20, 330, 150, 16))
        self.label_7.setMinimumSize(QtCore.QSize(150, 16))
        self.label_7.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.lineEdit_Platform_ver = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Platform_ver.setGeometry(QtCore.QRect(180, 330, 120, 20))
        self.lineEdit_Platform_ver.setMinimumSize(QtCore.QSize(120, 20))
        self.lineEdit_Platform_ver.setMaximumSize(QtCore.QSize(120, 20))
        self.lineEdit_Platform_ver.setObjectName("lineEdit_Platform_ver")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(20, 170, 150, 16))
        self.label_8.setMinimumSize(QtCore.QSize(150, 16))
        self.label_8.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(20, 250, 150, 16))
        self.label_9.setMinimumSize(QtCore.QSize(150, 16))
        self.label_9.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(20, 280, 150, 16))
        self.label_10.setMinimumSize(QtCore.QSize(150, 16))
        self.label_10.setMaximumSize(QtCore.QSize(150, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_10.setFont(font)
        self.label_10.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_10.setObjectName("label_10")
        self.lineEdit_DB_Path_Srvr = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_DB_Path_Srvr.setGeometry(QtCore.QRect(180, 280, 150, 20))
        self.lineEdit_DB_Path_Srvr.setMinimumSize(QtCore.QSize(150, 20))
        self.lineEdit_DB_Path_Srvr.setMaximumSize(QtCore.QSize(150, 20))
        self.lineEdit_DB_Path_Srvr.setText("")
        self.lineEdit_DB_Path_Srvr.setObjectName("lineEdit_DB_Path_Srvr")
        self.lineEdit_DB_Path_Srvr.setReadOnly(True)
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(340, 280, 140, 16))
        self.label_11.setMinimumSize(QtCore.QSize(140, 16))
        self.label_11.setMaximumSize(QtCore.QSize(140, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_11.setFont(font)
        self.label_11.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_11.setObjectName("label_11")
        self.lineEdit_DB_Path_Srvr_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_DB_Path_Srvr_2.setGeometry(QtCore.QRect(485, 280, 150, 20))
        self.lineEdit_DB_Path_Srvr_2.setMinimumSize(QtCore.QSize(150, 20))
        self.lineEdit_DB_Path_Srvr_2.setMaximumSize(QtCore.QSize(150, 20))
        self.lineEdit_DB_Path_Srvr_2.setText("")
        self.lineEdit_DB_Path_Srvr_2.setObjectName("lineEdit_DB_Path_Srvr_2")
        self.lineEdit_DB_Path_Srvr_2.setReadOnly(True)
        self.pushButton_DB_Name_Lock = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_DB_Name_Lock.setGeometry(QtCore.QRect(650, 270, 32, 32))
        self.pushButton_DB_Name_Lock.setMinimumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_Name_Lock.setMaximumSize(QtCore.QSize(32, 32))
        self.pushButton_DB_Name_Lock.setText("")
        self.pushButton_DB_Name_Lock.setObjectName("pushButton_DB_Name_Lock")
        self.pushButton_DB_Name_Lock.clicked.connect(lambda: self.lock_unlock(self.pushButton_DB_Name_Lock,
                                    self.lineEdit_DB_Path_Srvr_2, Linux, self.lineEdit_DB_Path_Srvr))

        self.pushButton_DB_Name_Lock.locked = True
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

        self.set_Icons(Linux)

        self.setWindowModality(Qt.Qt.ApplicationModal)

        self.passing_a_strings(currNode)

    def lock_unlock(self, btn, lineEdit, Linux, srvr=None):
        icon = QtGui.QIcon()
        if Linux:
            if btn.locked == True:
                icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/lock-open-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                btn.locked = False
                lineEdit.setReadOnly(False)
                if srvr != None:
                    srvr.setReadOnly(False)
            else:
                icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/lock-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                btn.locked = True
                lineEdit.setReadOnly(True)
                if srvr != None:
                    srvr.setReadOnly(True)
        else:
            if btn.locked == True:
                icon.addPixmap(QtGui.QPixmap(r":\AdminConsole\Icons\lock-open-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                btn.locked = False
                lineEdit.setReadOnly(False)
                if srvr != None:
                    srvr.setReadOnly(False)
            else:
                icon.addPixmap(QtGui.QPixmap(r":\AdminConsole\Icons\lock-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                btn.locked = True
                lineEdit.setReadOnly(True)
                if srvr != None:
                    srvr.setReadOnly(True)

            if btn.objectName() == 'pushButton_DB_PATH_Lock':
                if btn.locked == True:
                    self.pushButton_DB_Path_File.setEnabled(False)
                else:
                    self.pushButton_DB_Path_File.setEnabled(True)

        btn.setIcon(icon)

    def passing_a_strings(self, currNode):
        server_base = currNode.data(3, 0)
        if server_base[1:2] == ":":
            self.lineEdit_DB_Path_File.setText(server_base)
            self.lineEdit_IB_Name.setText(currNode.data(2, 0))
        else:
            self.lineEdit_DB_Path_Srvr.setText(server_base)
            self.lineEdit_DB_Path_Srvr_2.setText(currNode.data(4, 0))
            self.lineEdit_IB_Name.setText(currNode.data(2, 0))
        self.lineEdit_Start_Params.setText(currNode.data(6, 0))
        if currNode.data(9, 0) == "x86_64":
            self.comboBox.setCurrentText("64 (x86_64)")
        elif currNode.data(9, 0) == "x86":
            self.comboBox.setCurrentText("32 (x86)")
        elif currNode.data(9, 0) == "x86_prt":
            self.comboBox.setCurrentText("Приоритет 32 (x86)")
        elif currNode.data(9, 0) == "x86_64_prt":
            self.comboBox.setCurrentText("Приоритет 64 (x86_64)")
        self.comboBox_2.setCurrentText(currNode.data(17, 0))

    def pushButton_DB_Path_File_clicked(self):
        destDir = QtWidgets.QFileDialog.getExistingDirectory(self,
                                                     'Open working directory',
                                                     os.getenv('HOME'),
                                                     QtWidgets.QFileDialog.ShowDirsOnly)

        print(destDir)

    def set_Icons(self, Linux):
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/lock-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_IB_Name_Lock.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/lock-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_DB_PATH_Lock.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/lock-line.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_DB_Name_Lock.setIcon(icon)

    def open_prop_window(self, currNode):
        self.prop_window = ParametriZapuska(currNode, self.lineEdit_Start_Params)
        self.prop_window.show()

    def button_ok_clicked(self, currNode, Linux, parent):
        text_file_base = currNode.data(3, 0)
        new_text = self.lineEdit_DB_Path_File.text()
        if self.lineEdit_DB_Path_File.text() != '':
            if new_text != text_file_base:
                currNode.setData(3, 0, new_text)
        else:
            if self.lineEdit_DB_Path_Srvr.text() != currNode.data(3, 0):
                currNode.setData(3, 0, self.lineEdit_DB_Path_Srvr.text())
                currNode.setData(4, 0, self.lineEdit_DB_Path_Srvr_2.text())

        if self.lineEdit_Configuration.text() != currNode.data(21, 0):
            currNode.setData(21, 0, self.lineEdit_Configuration.text())
        if self.lineEdit_IB_Name.text() != currNode.data(2, 0):
            currNode.setData(2, 0, self.lineEdit_IB_Name.text())
        currNode.setData(6, 0, self.lineEdit_Start_Params.text())
        currNode.setData(9, 0, self.comboBox.currentText())
        currNode.setData(10, 0, self.lineEdit_Configuration.text())
        if self.comboBox_2.currentText() == "Выбирать автоматически":
            currNode.setData(17, 0, "Auto")
        elif self.comboBox_2.currentText() == "Веб-клиент":
            currNode.setData(17, 0, "WebClient")
        elif self.comboBox_2.currentText() == "Толстый клиент":
            currNode.setData(17, 0, "ThickClient")
        elif self.comboBox_2.currentText() == "Тонкий клиент":
            currNode.setData(17, 0, "ThinClient")
        if self.comboBox.currentText() == "64 (x86_64)":
            currNode.setData(9, 0, "x86_64")
        elif self.comboBox.currentText() == "32 (x86)":
            currNode.setData(9, 0, "x86")
        elif self.comboBox.currentText() == "Приоритет 32 (x86)":
            currNode.setData(9, 0, "x86_prt")
        elif self.comboBox.currentText() == "Приоритет 64 (x86_64)":
            currNode.setData(9, 0, "x86_64_prt")
        SaveData(Linux, parent)
        self.close()

    def button_cancel_clicked(self):
        self.close()

    def plat_ver_clicked(self, Linux):
        self.plat_ver_window = Plat_Version(Linux, self.lineEdit_Platform_ver)
        self.plat_ver_window.show()
        self.plat_ver_window.show()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Свойства базы"))
        self.label.setText(_translate("MainWindow", "Настройки"))
        self.label_2.setText(_translate("MainWindow", "Конфигурация:"))
        self.label_3.setText(_translate("MainWindow", "Имя базы:"))
        self.lineEdit_IB_Name.setToolTip(_translate("MainWindow", "Чтобы изменить это поле, нажмите \"замок\" справа"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Любая"))
        self.comboBox.setItemText(1, _translate("MainWindow", "64 (x86_64)"))
        self.comboBox.setItemText(2, _translate("MainWindow", "32 (x86)"))
        self.comboBox.setItemText(3, _translate("MainWindow", "Приоритет 64 (x86_64)"))
        self.comboBox.setItemText(4, _translate("MainWindow", "Приоритет 32 (x86)"))

        self.pushButtonOK.setText(_translate("MainWindow", "ОК"))

        self.pushButton_Cancel.setText(_translate("MainWindow", "Отмена"))

        self.pushButton_Plat_ver.setText(_translate("MainWindow", "..."))
        self.label_4.setText(_translate("MainWindow", "       Каталог базы:"))
        self.label_5.setText(_translate("MainWindow", "Режим запуска:"))
        self.lineEdit_DB_Path_File.setToolTip(_translate("MainWindow", "Чтобы изменить это поле, нажмите \"замок\" справа"))
        self.pushButton_IB_Name_Lock.setToolTip(_translate("MainWindow", "Включить возможность изменения имени базы"))
        self.pushButton_DB_PATH_Lock.setToolTip(_translate("MainWindow", "Включить возможность изменения пути базы"))
        self.comboBox_2.setItemText(0, _translate("MainWindow", "Выбирать автоматически"))
        self.comboBox_2.setItemText(1, _translate("MainWindow", "Веб-клиент"))
        self.comboBox_2.setItemText(2, _translate("MainWindow", "Тонкий клиент"))
        self.comboBox_2.setItemText(3, _translate("MainWindow", "Толстый клиент"))
        self.label_6.setText(_translate("MainWindow", "Параметры запуска:"))
        self.pushButton_Start_params.setText(_translate("MainWindow", "..."))
        self.label_7.setText(_translate("MainWindow", "Версия платформы:"))
        self.label_8.setText(_translate("MainWindow", "Файловая база:"))
        self.label_9.setText(_translate("MainWindow", "Серверная база:"))
        self.label_10.setText(_translate("MainWindow", "       Кластер серверов:"))
        self.lineEdit_DB_Path_Srvr.setToolTip(_translate("MainWindow", "Чтобы изменить это поле, нажмите \"замок\" справа"))
        self.label_11.setText(_translate("MainWindow", "Имя базы в кластере:"))
        self.lineEdit_DB_Path_Srvr_2.setToolTip(_translate("MainWindow", "Чтобы изменить это поле, нажмите \"замок\" справа"))
        self.pushButton_DB_Name_Lock.setToolTip(_translate("MainWindow", "Включить возможность изменения пути базы"))
